
$(document).ready(function() {
    $.getJSON("https://api.coinmarketcap.com/v1/ticker/?limit=10", function(data){
        populateExchangeVal(data);
    });
    setInterval(function() {
        $.getJSON("https://api.coinmarketcap.com/v1/ticker/?limit=4", function(data){
            populateExchangeVal(data);
        });
    },300000);
    
    function populateExchangeVal(data) {
        let excntr = $('#ExchangeDataContainer');
        $.each(excntr.find('.crypto-item'), function(key,val){ 
            $.each(data, function(x,y) {
                if($(val).attr('data-id') == y.id) {
                    $(val).find('.title').html( y.symbol + " -> BTC/USD");
                    $(val).find('.btcval').html( y.price_btc);
                    $(val).find('.usdval').html(y.price_usd);
                }       
            });
        })
    }

    $('.crypto-item-container').slick({
        slidesToShow: 2,
        centerPadding: "100px",
        infinite: false,
        adaptiveHeight: true  
    });

    $.getJSON('/ethhis', function(data) {
        Morris.Line({
            element: 'morris',
            data: data,
            xkey: 'UnixTimeStamp',
            xLabelFormat: function(d) {
                return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear(); 
            },
            // xLabels:'day',
            // labels: ['Winners', 'Non-winners', 'Total'],
            lineColors: ['#167f39','#990000', '#000099'],
            lineWidth: 2,
            dateFormat: function (ts) {
                var d = new Date(ts);
                return d.getYear() + '/' + (d.getMonth() + 1) + '/' + d.getDay();
            },
            ykeys: ['Value'],
            // labels: ['Value'],
        });
    });

    var chart = AmCharts.makeChart( "chartDiv", {
    "type": "serial",
    "theme": "light",
    "dataDateFormat":"YYYY-MM-DD",
    "valueAxes": [ {
        "position": "left"
    } ],
    "graphs": [ {
        "id": "g1",
        "balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
        "closeField": "close",
        "fillColors": "#7f8da9",
        "highField": "high",
        "lineColor": "#7f8da9",
        "lineAlpha": 1,
        "lowField": "low",
        "fillAlphas": 0.9,
        "negativeFillColors": "#db4c3c",
        "negativeLineColor": "#db4c3c",
        "openField": "open",
        "title": "Price:",
        "type": "candlestick",
        "valueField": "close"
    } ],
    "chartScrollbar": {
        "graph": "g1",
        "graphType": "line",
        "scrollbarHeight": 30
    },
    "chartCursor": {
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true
    },
    "dataProvider": getData(),

    "export": {
        "enabled": true,
        "position": "bottom-right"
    }
    } );
    function getData() {
        let data = [ {
            "date": "2011-08-01",
            "open": "136.65",
            "high": "136.96",
            "low": "134.15",
            "close": "136.49"
        }, {
            "date": "2011-08-02",
            "open": "135.26",
            "high": "135.95",
            "low": "131.50",
            "close": "131.85"
        }, {
            "date": "2011-08-05",
            "open": "132.90",
            "high": "135.27",
            "low": "128.30",
            "close": "135.25"
        }, {
            "date": "2011-08-06",
            "open": "134.94",
            "high": "137.24",
            "low": "132.63",
            "close": "135.03"
        }, {
            "date": "2011-08-07",
            "open": "136.76",
            "high": "136.86",
            "low": "132.00",
            "close": "134.01"
        }, {
            "date": "2011-08-21",
            "open": "131.22",
            "high": "132.75",
            "low": "130.33",
            "close": "132.51"
        },];

        return data;

    }


    chart.addListener( "rendered", zoomChart );
    zoomChart();

    // this method is called when chart is first inited as we listen for "dataUpdated" event
    function zoomChart() {
    // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
    chart.zoomToIndexes( chart.dataProvider.length - 10, chart.dataProvider.length - 1 );
    }   

    setInterval(function(){ 
        

        let newD = new Date(chart.dataProvider[chart.dataProvider.length -1].date);
        //- let newD = new Date(data[data.length-1].date);
        let newDate = newD.getDate() + 1;
        newD.setDate(newDate);

        

        chart.dataProvider.shift();
        chart.dataProvider.push( {
            "date": newD.toISOString(),
            "open": getRandomArbitrary(123.5, 135.5),
            "high": getRandomArbitrary(135.5, 145.5),
            "low": getRandomArbitrary(123.5, 135.5),
            "close": getRandomArbitrary(135.5, 145.5)
        } );
        chart.validateData(); newDate = newD.getDate() + 1;
        
        
    }, 2000);
    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }
});