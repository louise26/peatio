module Private
  class AssetsController < BaseController
    skip_before_action :auth_member!, only: [:index]

    def index
      @imc_assets  = Currency.assets('imc')
      @cbr_assets  = Currency.assets('cbr')
      @btc_proof   = Proof.current :btc
      @imc_proof   = Proof.current :imc
      @cbr_proof   = Proof.current :cbr

      if current_user
        @btc_account = current_user.accounts.with_currency(:btc).first
        @imc_account = current_user.accounts.with_currency(:imc).first
        @cbr_account = current_user.accounts.with_currency(:cbr).first
      end
    end

    def partial_tree
      account    = current_user.accounts.with_currency(params[:id]).first
      @timestamp = Proof.with_currency(params[:id]).last.timestamp
      @json      = account.partial_tree.to_json.html_safe
      respond_to do |format|
        format.js
      end
    end

  end
end
