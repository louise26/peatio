FactoryGirl.define do
  factory :fund_source do
    extra 'bitcoin'
    uid { Faker::Bitcoin.address }
    is_locked false
    currency 'btc'

    member { create(:member) }

    trait :imc do
      extra 'bc'
      uid '123412341234'
      currency 'imc'
    end

    factory :imc_fund_source, traits: [:imc]
    factory :btc_fund_source
  end
end

